import {capitalize} from '@vue/shared';

const capitalizeAll = (str: string): string => {
  return str.split('-').map(s => capitalize(s)).join(' ');
};

export default capitalizeAll;
