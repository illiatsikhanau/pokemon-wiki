import axios from 'axios';

const POKE_API_URL = 'https://pokeapi.co/api/v2';

export const replaceUrl = (url: string): string => {
  return url.replace(POKE_API_URL, '');
};

axios.interceptors.request.use(config => {
  return {...config, baseURL: POKE_API_URL};
});

export default axios;
