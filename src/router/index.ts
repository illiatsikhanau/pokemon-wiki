import {createRouter, createWebHistory} from 'vue-router';
import Home from '@/views/Home.vue';
import List from '@/views/List.vue';
import Pokemon from '@/views/Pokemon.vue';
import Ability from '@/views/Ability.vue';
import Move from '@/views/Move.vue';
import Search from '@/views/Search.vue';

const router = createRouter({
  history: createWebHistory(),
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    }, {
      path: '/pokemons/',
      name: 'pokemons',
      component: List,
      props: {
        listName: 'pokemons'
      },
    }, {
      path: '/pokemon/:id',
      name: 'pokemon',
      component: Pokemon,
    }, {
      path: '/abilities/',
      name: 'abilities',
      component: List,
      props: {
        listName: 'abilities'
      },
    }, {
      path: '/ability/:id',
      name: 'ability',
      component: Ability,
    }, {
      path: '/moves/',
      name: 'moves',
      component: List,
      props: {
        listName: 'moves'
      },
    }, {
      path: '/move/:id',
      name: 'move',
      component: Move,
    }, {
      path: '/search/:name?',
      name: 'search',
      component: Search,
    },
  ]
});

export default router;
