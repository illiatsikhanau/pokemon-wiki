import {defineStore} from 'pinia';
import type {Resource} from '@/types/Resource';

export const useMoveStore = defineStore('move', {
  state: () => ({
    list: [] as Resource[],
  }),
  actions: {
    setList(movesList: Resource[]) {
      this.list = movesList;
    },
  },
});
