import {defineStore} from 'pinia';
import type {Resource} from '@/types/Resource';

export const usePokemonStore = defineStore('pokemon', {
  state: () => ({
    list: [] as Resource[],
  }),
  actions: {
    setList(pokemonsList: Resource[]) {
      this.list = pokemonsList;
    },
  },
});
