import {defineStore} from 'pinia';
import type {Resource} from '@/types/Resource';

export const useAbilityStore = defineStore('ability', {
  state: () => ({
    list: [] as Resource[],
  }),
  actions: {
    setList(abilitiesList: Resource[]) {
      this.list = abilitiesList;
    },
  },
});
